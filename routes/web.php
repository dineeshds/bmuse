<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/


Route::get('/','Test\SubscribersController@newIndex');
Route::resource('subscribers', 'Test\SubscribersController');
Route::get('new-subscriber', 'Test\SubscribersController@newIndex');
Route::post('create-new-subscriber', 'Test\SubscribersController@createNewSubscriber');
