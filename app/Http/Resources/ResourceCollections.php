<?php

namespace App\Http\Resources;

use Illuminate\{Http\JsonResponse, Http\Request, Http\Resources\Json\ResourceCollection, Support\Collection};

/**
 * @method total()
 * @method perPage()
 * @method currentPage()
 * @method lastPage()
 */
class ResourceCollections extends ResourceCollection
{
    public $pagination;

    public $theResource;

    public $connection;

    public function __construct($resource, $theResource, $pagination = true, $connection = [] )
    {
        parent::__construct($resource);

        $this->pagination = $pagination;

        $this->theResource = $theResource;

        $this->connection = $connection;
    }

    /**
     * Transform the resource collection into an array.
     *
     * @param  Request  $request
     * @return array
     */
    public function toArray($request)
    {
        return $this->merges(new Collection([$this->theResource::collection($this->collection)->resolve($this->connection)]));
    }

    /**
     * Merge all Function as Array then return
     *
     * @param $data
     * @return mixed
     */
    private function merges($data)
    {
//        if ($this->pagination)
//        {
//            $data = $data->merge($this->pagination());
//        }
        return $data;
    }

    /**
     * @return Collection
     */
    private function pagination()
    {
        return new Collection([
            'pagination'    => [
                 'total'         => $this->total(),
                 'count'         => $this->count(),
                 'per_page'      => $this->perPage(),
                 'current_page'  => $this->currentPage(),
                 'total_pages'   => $this->lastPage()
            ],
        ]);
    }

    /**
     * @param Request $request
     * @param JsonResponse $response
     */
    public function withResponse($request, $response)
    {
        $jsonResponse = json_decode($response->getContent(), true);
        unset($jsonResponse['links'],$jsonResponse['meta']);
        $response->setContent(json_encode($jsonResponse));
    }
}
