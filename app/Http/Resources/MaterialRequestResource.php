<?php

namespace App\Http\Resources;

use App\{Http\Repositories\MaterialRequestRepository,
    Http\Resources\Inventory\Item\ItemResource,
    Traits\ResourceHelpers};
use Illuminate\{Database\Eloquent\Builder,
    Database\Eloquent\Model,
    Http\Request,
    Http\Resources\Json\JsonResource,
    Support\Collection};

class MaterialRequestResource extends JsonResource
{
    use ResourceHelpers;

    private $haystack;

    /**
     * @var Builder|Builder[]|\Illuminate\Database\Eloquent\Collection|Model|null
     */
    private $material;

    /**
     * @return Builder|Builder[]|\Illuminate\Database\Eloquent\Collection|Model|null
     */
    public function getMaterial()
    {
        return $this->material;
    }

    /**
     * MaterialRequestResource constructor.
     * @param $resource
     */
    public function __construct($resource)
    {
        parent::__construct($resource);
        $this->material = MaterialRequestRepository::findMaterial($this->id);
    }

    /**
     * Transform the resource into an array.
     *
     * @param Request $request
     * @return Collection
     */
    public function toArray($request): Collection
    {
        $this->haystack = (isset($request->get))  ? explode( ',', $request->get) : ['all'];
        $material = new Collection([
            'id'                    => $this->id,
            'name'                  => $this->name,

        ]);
        return $material;
    }




}
