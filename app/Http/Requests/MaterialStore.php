<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class MaterialStore extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'series'                    => ['required', 'string'],
            'type'                      => ['required', 'in:purchase,material_transfer,material_issue,manufacture,customer_provided'],
            'customer_id'               => ['nullable', 'exists:'. app('database-connection') .'.inv_customers,id'],
            'required_date'             => ['nullable', 'date'],
            'requested_for'             => ['nullable', 'email'],
            'transaction_date'          => ['required', 'date'],
            'employee_id'              => ['nullable', 'exists:'. app('database-connection') .'.employees,id'],
            'letter_head'               => ['nullable', 'string'],
            'print_heading'             => ['nullable', 'string'],
            'terms'                     => ['nullable', 'string'],
            'terms_and_conditions'      => ['nullable', 'string'],
            'items'                     => ['required', 'array'],
            'items.*.item_id'           => ['required', 'exists:'. app('database-connection') .'.inv_item,id'],
            'items.*.quantity'          => ['required', 'numeric'],
            'items.*.uom_id'            => ['required', 'exists:'. app('database-connection') .'.inv_uom,id'],
            'items.*.warehouse_id'      => ['nullable', 'string'],
            'items.*.for_warehouse_id'   => ['nullable', 'integer', 'exists:' . app('database-connection') . '.inv_stock_warehouse,id'],
            'items.*.required_date'     => ['required', 'date'],
            'items.*.item_name'         => ['nullable', 'string'],
            'items.*.description'       => ['required', 'string'],
            'items.*.manufacturer'      => ['nullable', 'string'],
            'items.*.manufacturer_part_number' => ['nullable', 'string'],
            'items.*.rate'              => ['nullable', 'numeric'],
            'items.*.uom_conversion_factor' => ['required', 'numeric'],
            'items.*.stock_uom'         => ['nullable'],
            'items.*.expense_account'   => ['nullable', 'string'],
            'items.*.project_id'        => ['nullable', 'exists:'. app('database-connection') .'.inv_project,id'],
            'items.*.cost_center'       => ['nullable', 'string'],
            'items.*.page_break'        => ['nullable', 'boolean'],
        ];
    }
}
