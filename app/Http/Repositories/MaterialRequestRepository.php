<?php

namespace App\Http\Repositories;

use App\{Http\Repositories\HelperContractRepository,
    Models\MaterialRequest,
    Models\Inventory\Material\MaterialItem,
    Models\Inventory\Item\Item,
    Traits\FilesHelper,
    Traits\ResourceHelpers};
use Illuminate\{Contracts\Pagination\LengthAwarePaginator,
    Database\Eloquent\Builder,
    Database\Eloquent\Collection,
    Database\Eloquent\Model};

class MaterialRequestRepository extends HelperContractRepository
{
    use ResourceHelpers, FilesHelper;

    public function __construct(MaterialRequest $material)
    {
        $this->model = $material;
    }

    /**
     * Get all the model records in the database.
     *
     * @param array $with
     * @return LengthAwarePaginator|mixed
     */
    public function all($with = [])
    {
        if ($projectId = request('project_id')) {
            return $this->model::where('project_id', $projectId)->get();
        }
        return $this->model::all();
    }

    /**
     * @param array $data
     * @return bool
     */
    public function store(array $data)
    {
        $material = $this->model::create([
            'series'                => $data['series'],
            'type'                  => $data['type'],
            'customer_id'           => $data['customer_id'] ?? null,
            'required_date'         => $data['required_date'] ?? null,
            'requested_for'         => $data['requested_for'] ?? null,
            'transaction_date'      => $data['transaction_date'],
            'requested_by'          => $data['employee_id'] ?? null,
            'letter_head'           => $data['letter_head'] ?? null,
            'print_heading'         => $data['print_heading'] ?? null,
            'terms'                 => $data['terms'] ?? null,
            'terms_and_conditions'  => $data['terms_and_conditions'] ?? null,
        ]);
        $this->StoreUpdateItems($data['items'] ?? [], $material->id);
        return $material;
    }

    /**
     * @param array $items
     * @param $materialId
     * @param bool $update
     */
    private function StoreUpdateItems(array $items, $materialId, $update = false)
    {
        if ($update) {
            foreach (MaterialItem::where('material_request_id', $materialId)->get() as $row) {
                $row->delete();
            }
        }

        foreach($items as $item) {
            MaterialItem::create([
                'material_request_id'       => $materialId,
                'item_id'                   => $item['item_id'],
                'quantity'                  => $item['quantity'],
                'uom_id'                    => $item['uom_id'],
                'for_warehouse_id'             => $item['for_warehouse_id'] ?? null,
                'required_date'             => $item['required_date'],
                'item_name'                 => $item['item_name'] ?? null,
                'description'               => $item['description'],
                'manufacturer'              => $item['manufacturer'] ?? null,
                'manufacturer_part_number'  => $item['manufacturer_part_number'] ?? null,
                'rate'                      => $item['rate'] ?? null,
                'uom_conversion_factor'     => $item['uom_conversion_factor'],
                'stock_uom'                 => $item['stock_uom'] ?? null,
                'expense_account'           => $item['expense_account'] ?? null,
                'project_id'                => $item['project_id'] ?? null,
                'cost_center_id'            => $item['cost_center_id'] ?? null,
                'page_break'                => $item['page_break'] ?? 0,
            ]);
        }
    }
    /**
     * @param array $data
     * @param MaterialRequest $material
     * @return Model
     */
    public function update(array $data, MaterialRequest $material)
    {
        $material->update([
            'series'                => $data['series'],
            'type'                  => $data['type'],
            'customer_id'           => $data['customer_id'] ?? null,
            'required_date'         => $data['required_date'] ?? null,
            'requested_for'         => $data['requested_for'] ?? null,
            'transaction_date'      => $data['transaction_date'],
            'requested_by'          => $data['employee_id'] ?? null,
            'letter_head'           => $data['letter_head'] ?? null,
            'print_heading'         => $data['print_heading'] ?? null,
            'terms'                 => $data['terms'] ?? null,
            'terms_and_conditions'  => $data['terms_and_conditions'] ?? null,
        ]);
        $this->StoreUpdateItems($data['items'] ?? [], $material->id, true);
        return $this->find($material->id, ['items']);
    }

    /**
     * @param int $materialId
     * @param array $with
     * @return Builder|Builder[]|Collection|Model|null
     */
    public function find(int $materialId, $with = [])
    {
        return self::findMaterial($materialId, $with);
    }

    /**
     * @param int $id
     * @param array $with
     * @return Builder|Builder[]|Collection|Model|null
     */
    public static function findMaterial(int $id, $with = [])
    {
        return MaterialRequest::with($with)->find($id);
    }

    /**
     * @return MaterialItem[]|Collection
     */
    public function getRequestedItems()
    {
        return MaterialItem::all();
    }


    public function getItemTotal()
    {
        return MaterialItem::select(\DB::raw("sum(quantity) as quantity"),
            \DB::raw("sum(quantity) as qty_to_order"),
            \DB::raw("sum(0) as ordered_qty")
            )->first();
    }
}
