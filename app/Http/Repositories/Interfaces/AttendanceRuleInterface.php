<?php

namespace App\Http\Repositories\Interfaces;

use App\Models\HR\Attendance\AttendanceRule;

interface AttendanceRuleInterface
{
    /**
     * @param $count
     * @param $type
     * @return mixed
     */
    public function all($count, $type);

    /**
     * @param array $data
     * @return bool
     */
    public function store(array $data);

    /**
     * @param array $data
     * @param AttendanceRule $attendanceRule
     * @return mixed
     */
    public function update(array $data, AttendanceRule $attendanceRule);

    /**
     * @param int $attendanceRuleId
     * @return mixed
     */
    public function find(int $attendanceRuleId);

    /**
     * @param AttendanceRule $attendanceRule
     * @return mixed
     */
    public function delete(AttendanceRule $attendanceRule);

}
