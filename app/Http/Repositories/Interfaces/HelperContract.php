<?php

namespace App\Http\Repositories\Interfaces;

interface HelperContract
{
    /**
     * @return mixed
     */
    public function all();

    /**
     * @return mixed
     */
    public function count();

    /**
     * @param $id
     * @return mixed
     */
    public function deleteById($id);

    /**
     * @return mixed
     */
    public function first();

    /**
     * @return mixed
     */
    public function get();

    /**
     * @param $id
     * @return mixed
     */
    public function getById($id);

    /**
     * @param $item
     * @param $column
     * @param array $columns
     * @return mixed
     */
    public function getByColumn($item, $column, array $columns = ['*']);

    /**
     * @param $limit
     * @return mixed
     */
    public function limit($limit);

    /**
     * @param $column
     * @param $value
     * @return mixed
     */
    public function orderBy($column, $value);

    /**
     * @param int $limit
     * @param array $columns
     * @param string $pageName
     * @param null $page
     * @return mixed
     */
    public function paginate($limit = 25, array $columns = ['*'], $pageName = 'page', $page = null);

    /**
     * @param $column
     * @param $value
     * @param string $operator
     * @return mixed
     */
    public function where($column, $value, $operator = '=');

    /**
     * @param $column
     * @param $value
     * @return mixed
     */
    public function whereIn($column, $value);

    /**
     * @param $relations
     * @return mixed
     */
    public function with($relations);
}
