<?php

namespace App\Http\Controllers\Auth;

use App\Models\EmploymentType;
use App\Models\Enrollment;
use App\Http\Controllers\Controller;
use App\Models\Gender;
use App\Models\GroupInfo;
use App\Models\GroupUser;
use App\Models\UserProfile;
use App\Providers\RouteServiceProvider;
use App\Traits\BaseHelper;
use App\User;
use Illuminate\Foundation\Auth\RegistersUsers;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;

class RegisterController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Register Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles the registration of new users as well as their
    | validation and creation. By default this controller uses a trait to
    | provide this functionality without requiring any additional code.
    |
    */

    use RegistersUsers, BaseHelper;

    /**
     * Where to redirect users after registration.
     *
     * @var string
     */
    protected $redirectTo = RouteServiceProvider::HOME;

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest');
    }

    /**
     * Show the application registration form.
     *
     * @return \Illuminate\View\View
     */
    public function showRegistrationForm()
    {
        $gender = Gender::all();
        $employmentType = EmploymentType::all();
        return view('auth.register', compact('gender', 'employmentType'));
    }

    /**
     * Get a validator for an incoming registration request.
     *
     * @param  array  $data
     * @return \Illuminate\Contracts\Validation\Validator
     */
    protected function validator(array $data)
    {
        $data['phone']= $this->removeMask($data['phone']);

        return Validator::make($data, [
            'first_name' => ['required', 'string', 'max:255'],
            'email' => ['required', 'string', 'email', 'max:255', 'unique:users'],
            'password' => ['required', 'string', 'min:8', 'confirmed'],
            'phone' => ['required', 'unique:users'],
            'group_code' => ['required', 'exists:group_infos,code'],
            'gender_id' => ['required', 'exists:genders,id']
        ]);
    }

    /**
     * Create a new user instance after a valid registration.
     *
     * @param  array  $data
     * @return \App\User
     */
    protected function create(array $data)
    {
        $groupId = GroupInfo::getIdByCode($data['group_code']);
        $enrollmentNumber = $this->generateSeriesNumber('EN', Enrollment::class, 'en_number');
        $phone = str_replace(['(', ')', '-', ' '], '',$data['phone'] );
        $userProfile = UserProfile::create([
            'gender_id' => $data['gender_id'],
            'dob'       => $data['dob'],
            'employment_type_id' => $data['employment_type']
        ]);
        $user = User::create([
            'user_profile_id' => $userProfile['id'],
            'name' => $data['first_name'],
            'first_name' => $data['first_name'],
            'middle_name' => $data['middle_name'] ?? null,
            'last_name' => $data['last_name'] ?? null,
            'email' => $data['email'],
            'password' => Hash::make($data['password']),
            'phone' => $phone ?? null,
            'masked_phone' => $data['phone']
        ]);
        $user->assignRole('primary_user');
        GroupUser::create([
            'user_id' => $user['id'],
            'group_id' => $groupId
        ]);
        $enrollment = new Enrollment();
        $enrollment->en_number = $enrollmentNumber;
        $enrollment->user_id = $user->id;
        $enrollment->group_id = $groupId;
        $enrollment->status = 0;
        $enrollment->save();

        return $user;
    }

    public function checkGroupCode(Request $request)
    {
        $group = $request['group'];
        if($group){
           $isValid = GroupInfo::where('code', $group)->count();
           if($isValid !=0){
                $output = array(['success' => true]);
                return json_encode($output);
           }
        }
    }
}
