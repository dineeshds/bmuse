<?php

namespace App\Http\Controllers\Test;

use App\Http\Controllers\Controller;
use App\Mail\NewsLetter;
use App\Models\Subscriber;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;
use Illuminate\Support\Facades\Validator;
use Yajra\DataTables\Facades\DataTables;

class SubscribersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view('test.index');
    }

    /**
     * @return \Illuminate\Contracts\Foundation\Application|\Illuminate\Contracts\View\Factory|\Illuminate\Contracts\View\View|\Illuminate\View\View
     */
    public function newIndex()
    {
        return view('test.create');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $query = Subscriber::all();

        return Datatables::of($query)

            ->addColumn('action', function ( $query) {
                return
                    '<button type="button" class="btn btn-xs btn-warning btn-danger"
                    data-url="'. route('subscribers.destroy', $query->id). '" data-value="1">
                    <i class="fa fa-trash"></i> Delete</button>
                    <button type="button" class="btn btn-xs btn-primary" data-toggle="modal"
                    data-target="#edit-modal-default" onclick="newsletter('.$query->id.')">
                        <i class="fa fa-edit"></i> Send Newsletter</button>';
            })
            ->rawColumns(['action'])
            ->make(true);
    }

    public function createNewSubscriber(Request $request)
    {
        if($request->has('sub')){

            $request = $request['sub'];
            $sub = new Subscriber();
            $sub->name = $request['name'];
            $sub->email = $request['email'];
            $sub->save();
        }
        return redirect()->back()->with('alert', ['type' => 'success', 'message' => 'Successfully subscribed to news letter']);

    }
    /**
     * Store a newly created resource in storage. THis will send email to subscriber
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $subId = (int)$request['letter']['sub_id'];
        $subscriber = Subscriber::findOrFail($subId);

        $mailData = [
            'subject' => $request['letter']['subject'],
            'body' => $request['letter']['body'],
            'name' => $subscriber['name'] ?? 'No name',
            ];

        Mail::to($subscriber['email'])
            ->send(new NewsLetter($mailData));

        return redirect()->back()->with('alert', ['type' => 'success', 'message' => 'News letter email send successfully']);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Request $request, Subscriber $subscriber)
    {
        $subscriber->delete();

        return $this->apiResponse($subscriber);
    }
}
