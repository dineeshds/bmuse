<?php

namespace App\Http\Controllers;

use Illuminate\Foundation\Auth\Access\AuthorizesRequests;
use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\JsonResponse;
use Illuminate\Routing\Controller as BaseController;

class Controller extends BaseController
{
    use AuthorizesRequests, DispatchesJobs, ValidatesRequests;

    /**
     * @param $data
     * @param string $message
     * @param int $status
     * @return JsonResponse
     */
    protected function apiResponse($data, $message = '', $status = 200)
    {
        return response()->json(
            [
                'status'    =>  $status,
                'message'   =>  $message,
                'data'      =>  $data
            ]
        );
    }
}
