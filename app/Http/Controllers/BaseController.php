<?php

namespace App\Http\Controllers;

use App\{Http\Repositories\Interfaces\HelperContract,
    Http\Resources\ResourceCollections,
    Responses\GeneralResponse,
    Traits\BaseHelper};
use Illuminate\{Contracts\Foundation\Application, Http\JsonResponse, Http\RedirectResponse};

class BaseController extends Controller
{
    use BaseHelper;

    // determine which route we need
    protected $route;

    // determine which view we use
    protected $view;

    // determine which repository we use
    protected $model;

    // determine which repository we use
    protected $with = [];

    // determine which Response Interface We Use To Control Response In Index Function (Illuminate\Contracts\Support\Responsible)
    protected $indexResponse = GeneralResponse::class;

    // determine which FormRequest Use In Store
    protected $storeRequestFile;

    // determine which FormRequest Use In Update
    protected $updateRequestFile;

    // determine the resource use it
    protected $theResource;

    // determine the count of all
    protected $count = 1000;

    protected $id = null;

    protected $seriesType;
    /**
     * BaseController constructor.
     * @param HelperContract $repository
     */
    public function __construct(HelperContract $repository)
    {
        $this->model = $repository;
    }

    /**
     * @return Application|mixed
     */
    public function index()
    {
        return app($this->indexResponse, ['data' => new ResourceCollections($this->model->all(), $this->theResource, false), 'message' => 'Request success, get all']);
    }

    /**
     * @return JsonResponse
     */
    public function store()
    {
        app($this->storeRequestFile);

        if ($row =  $this->model->store(request()->all())) {
            return app($this->indexResponse, ['data' => new ResourceCollections([$row], $this->theResource,false), 'message' => 'Request success, Added new']);
        }
        return app($this->indexResponse, ['status' => 422, 'message' => 'Request failed, Failed add']);
    }

    /**
     * @param $id
     * @return JsonResponse
     */
    public function update(int $id)
    {
        app($this->updateRequestFile);

        if ($row = $this->model->getById($id)) {
            if ($row = $this->model->update(request()->all(), $row)) {
                return app($this->indexResponse, ['data' => [$row], 'message' => 'Request success, Updated']);
            }
        }
        return app($this->indexResponse, ['status' => 422, 'message' => 'Request failed, Failed update']);

    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return JsonResponse
     */
    public function show(int $id)
    {
        if (!$id) {
            $id = $this->id;
        }
        if ($row = $this->model->find($id, $this->with)) {
            return app($this->indexResponse, ['data' => new ResourceCollections([$row], $this->theResource,false), 'message' => 'Request success, get specific one']);
        }
        return app($this->indexResponse, ['message' => 'Request failed, cannot found this specific one', 'status' => 422]);
    }

    /**
     * @param int $id
     * @return Application|RedirectResponse|mixed|string
     */
    public function destroy(int $id)
    {
        if ($row =  $this->model->find($id)) {
            if ($this->model->deleteById($row->id)) {
                if (!strpos(request()->getUri(), '/api') !== false) {
                    return redirect()->back();
                }
                return app($this->indexResponse, ['message' => 'Request success, Delete specific one']);
            }
            return app($this->indexResponse, ['message' => 'Request Failed, delete this specific one', 'status' => 422]);
        }
        return app($this->indexResponse, ['message' => 'Request failed, cannot found this specific one', 'status' => 422]);
    }

}
