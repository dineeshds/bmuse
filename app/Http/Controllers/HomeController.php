<?php

namespace App\Http\Controllers;

use App\Models\Address;
use App\Models\Enrollment;
use App\Traits\BaseHelper;
use Illuminate\Http\Request;
use App\Models\Tier;
use App\Models\State;
use App\Models\UserProfile;

// use App\Http\Request\Enrollment\HomeRequest;

class HomeController extends Controller
{
    use BaseHelper;
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        $enrollment = Enrollment::getCurrentEnrollment();
        $page = $this->getEnrollmentState($enrollment);
        if($page != 'home'){
//            return redirect('/dashboard');
        }
        $tier = Tier::all();
        $state = State::get();

        $profile = auth()->user()->userProfile;
        $useraddress = $profile->address;

        return view('Enrollment.Plan.planHome', compact('tier', 'state', 'profile','useraddress'));
    }

}

