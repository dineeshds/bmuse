<?php

namespace App\Responses;

use App\Traits\Exceptions\RestTrait;
use Illuminate\{Contracts\Foundation\Application,
    Contracts\Support\Responsable,
    Contracts\View\Factory,
    Http\Request,
    View\View};
use Symfony\Component\HttpFoundation\Response;

class GeneralResponse implements Responsable
{
    use RestTrait;

    protected $status;

    protected $message;

    protected $data;

    protected $view;

    public function __construct($data = [], $message = '', $status = 200, $view = null)
    {
        $this->status   = $status;
        $this->message  = $message;
        $this->data     = $data;
        $this->view     = $view;
    }

    /**
     * @param Request $request
     * @return array|Application|Factory|View|Response
     */
    public function toResponse($request)
    {
        return [
            'status'    =>  $this->status,
            'message'   =>  $this->message,
            'data'      =>  $this->data
        ];
    }
}
