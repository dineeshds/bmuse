<?php

namespace App\Traits;

use App\Models\EnrollmentDetail;
use App\Models\PlanQuestion;
use Carbon\Carbon;

trait BaseHelper
{
    /**
     * @return array
     */
    public function getCompany()
    {
        $user = auth()->user()->userGroup()->first();
        if($user && $user->group()->exists()){
            return $user->group;
        }else {
            return [];
        }
    }

    /**
     * @param $plan
     * @return int|string
     */
    public function planTotal($plan)
    {
        $addons = $plan->plan->planPricingAddons;
        $baseRate = $plan->price ?? 0;
        $sub = $addons->where('type', 0)->sum('price') ?? 0;
        $add = $addons->where('type', 1)->sum('price') ?? 0;
        if(is_numeric($baseRate) && is_numeric($sub) && is_numeric($add)){
            return (($baseRate+$add)-$sub);
        }else{
            return 0;
        }
    }

    /**
     * @param $plan
     * @return int
     */
    public function planSurchargeTotal($plan)
    {
        $addons = $plan->plan->planPricingAddons;
        return $addons->where('type', 1)->sum('price') ?? 0;
    }

    /**
     * @param $plan
     * @return int
     */
    public function planSaving($plan)
    {
        $addons = $plan->plan->planPricingAddons;
        return $addons->where('type', 0)->sum('price') ?? 0;
    }

    /**
     * @param $plan
     * @return int
     */
    public function getPlanSurcharge($plan)
    {
        return $plan->planPricingAddons()->where('type', 1)->sum('price') ?? 0;
    }

    /**
     * @param $plan
     * @return int
     */
    public function getPlanSaving($plan)
    {
        return $plan->planPricingAddons()->where('type', 0)->sum('price') ?? 0;
    }

    /**
     * @param $suffix
     * @param $model
     * @param string $attribute
     * @return string
     */
    public function generateSeriesNumber($suffix , $model ,$attribute = 'id')
    {
        $text = $suffix.'.YYYY.-';
        $year = Carbon::now()->format('y');
        $dateReplace = str_replace('.YYYY.', $year, $text);

        $search = $this->searchTerm($dateReplace);
        // $modelData = $model::where($attribute, 'like', '%'. $search .'%')->withTrashed()->latest()->first();

        $modelData = $model::where($attribute, 'like', '%'. $search .'%')->latest()->first();
        if($modelData) {
            $lastSeries = $modelData->$attribute;
            $lastKey = $this->lastKey($lastSeries);
            $series = $this->uniqueKey($dateReplace, $lastKey);
        } else {
            $series = $dateReplace.'00001';
        }
        return $series;
    }

    /**
     * @param $text
     * @return string
     */
    public function searchTerm($text)
    {
        $exp = explode('-', $text);
        return $exp[0].'-'.$exp[1];
    }

    /**
     * @param $lastSeries
     * @return mixed
     */
    public function lastKey($lastSeries)
    {
        $exp = explode('-', $lastSeries);
        return $exp[count($exp)-1];
    }

    /**
     * @param $prefix
     * @param $lastKey
     * @return string
     */
    public function uniqueKey($prefix, $lastKey)
    {
        $newKey = $lastKey+1;
        $code = sprintf('%05d', $newKey);
        return $prefix.$code;
    }

    /**
     * @param $enrollment
     * @return string
     */
    public function getEnrollmentState($enrollment)
    {
        if($enrollment['filter_info'] ==0){
            return 'home';
        }else {
            return 'dashboard';
        }
    }

    /**
     * @param $enrollment
     */
    public function getRedirectUrl($enrollment)
    {
        if($enrollment['filter_info'] == 0){
            return 'home';
        }
        if($enrollment['dependent_info'] ==0){
            return 'dependent-info';
        }
        if($enrollment['enrollment_formdata'] ==0){
            return 'plan-overview';
        }

        if($enrollment['questionnaire'] ==0){
            return 'plan-questionnaire';
        }
        if($enrollment['beneficiary_info'] ==0){
            return 'plan-beneficiary';
        }
        if($enrollment['review_info'] ==0){
            return 'plan-review';
        }
        if($enrollment['agreement_info'] ==0){
            return 'agreements';
        }
        return 'home';

    }

    public function linkTitle($url)
    {
        if($url == 'home'){
            return 'Home page';
        }
        if($url == 'dependent-info'){
            return 'Dependent information page';
        }
        if($url == 'plan-overview'){
            return 'View plan page';
        }
        if($url == 'plan-questionnaire'){
            return 'Questionnaire page';
        }
        if($url == 'plan-beneficiary'){
            return 'Beneficiary page';
        }
        if($url == 'plan-review'){
            return 'Review page';
        }
        if($url == 'agreements'){
            return 'Agreement page';
        }
        return 'Home page';
    }

    /**
     * @param $phone
     * @return string|string[]
     */
    public function removeMask($phone)
    {
        return str_replace(['(', ')', '-', ' '], '',$phone);
    }

    public function mapQuestionnaireToPlan($enrollment, $answers)
    {
        $planPricing = $enrollment->enrollmentDetails()->with(['planPricing'])->get();
        foreach ($planPricing as $pricing) {
            $formated = [];
            $planQuestions = PlanQuestion::where('plan_id', $pricing['planPricing']['plan_id'])->with(['question'])->pluck('questionnaire_id');
            foreach ($planQuestions as $question) {

                if(array_key_exists($question, $answers)){
                    $formated[$question] = $answers[$question];
                }else if(array_key_exists($question, ($answers['multiple'] ?? []))){
                    $formated[$question] = $answers['multiple'][$question];
                }else if(array_key_exists($question, ($answers['textarea'] ?? []))){
                    $formated[$question] = $answers['textarea'][$question];
                }

            }
//            dd($planQuestions, $answers, $formated);
            $details = EnrollmentDetail::where('enrollment_id', $enrollment->id)
                ->where('plan_pricing_id', $pricing['planPricing']['id'])->first();
            $details->questionnaire_result = json_encode($formated);
            $details->save();

        }
        return;
    }
}
