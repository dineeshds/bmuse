<?php

namespace App\Traits;

use Carbon\Carbon;
use Illuminate\Support\Collection;
use function GuzzleHttp\Psr7\_parse_request_uri;

trait ResourceHelpers
{
    /**
     * @param Collection $collection
     * @param array $names
     * @return Collection
     */
    private function keyBy(Collection $collection, array $names)
    {
        return $collection->keyBy(function ($value, $key) use ($names) {
            if (array_key_exists($key, $names)) {
                return $names[$key];
            } else {
                return $key;
            }
        });
    }

    /**
     * @param $date
     * @param string $input
     * @param string $output
     * @return string|null
     */
    private function dateToDatabase($date, $input = 'd/m/Y', $output = 'Y-m-d')
    {
        if (is_null($date)) {
            return null;
        }
        return Carbon::createFromFormat($input, $date)->format($output);
    }

    /**
     * @param $time
     * @return string
     */
    private function timeToDatabase($time)
    {
        if (is_null($time)) {
            return null;
        }
        return Carbon::parse($time)->format('H:i:s');
    }

    /**
     * @param $id
     * @return int|mixed|string
     */
    public function changeIdShape($id)
    {
        //return ($id < 999)? str_pad($id, 2, '0', STR_PAD_LEFT) : ($id < 99)? str_pad($id, 3, '0', STR_PAD_LEFT):($id < 9)?str_pad($id, 4, '0', STR_PAD_LEFT):$id;
    }
}
