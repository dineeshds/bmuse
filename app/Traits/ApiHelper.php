<?php

namespace App\Traits;
use GuzzleHttp\Exception\ClientException;
use Illuminate\Support\Facades\Http;

trait ApiHelper
{
    public function getApi($zip)
    {
        $header = $this->getHeaders();
        $url = $this->getUrl().'/'.$zip;


//       $a =  Http::get($url);
        dd($url);
        $client = new \GuzzleHttp\Client(['headers' => $header]);
        try {
            $request = $client->get($url);
        } catch (ClientException $e) {
            $body = $e->getResponse()->getBody()->getContents();
            return $body;
        }
        $body = $request->getBody();
dd($body);
return $body;
    }

    public function getHeaders()
    {
        $header = [
            "Authorization" => config('seamlesschex.seamlesschex_paynote_sk'),
            "Content-Type" => " application/json"
        ];
        return $header;
    }

    public function getUrl()
    {
        $url = 'https://zip.getziptastic.com/v2/US';

        return $url;
    }

    public function getLocationByZip($zip)
    {
        $url = config('general.zip_validator').'/'.$zip;

        try {
            $request = Http::get($url);
        } catch (ClientException $e) {
            $body = $e->getResponse()->getBody()->getContents();
            return $body;
        }
        return $request->json();
    }
}
