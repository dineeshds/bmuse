<?php

use App\Models\Enrollment;
use App\Models\UserProfile;
use App\Traits\BaseHelper;
use Illuminate\Database\Seeder;
use Spatie\Permission\Models\Role;
use Spatie\Permission\Models\Permission;
use App\User;

class GeneralSeeder extends Seeder
{
    use BaseHelper;
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $enrollmentNumber = $this->generateSeriesNumber('EN', Enrollment::class, 'en_number');
        $permissions = [
            'user-list',
            'plan',
            'dashboard',
        ];
        foreach ($permissions as $permission) {
            Permission::updateOrCreate(['name' => $permission]);
        }
        $userProfile = UserProfile::create([
            'gender_id' => 1,
            'dob'       => NULL,
            'employment_type_id' => 1
        ]);
        $user = User::create([
            'user_profile_id' => $userProfile->id,
            'name' => 'admin',
            'email' => 'dineesh@anjamerica.com',
            'first_name' => 'admin',
            'middle_name' => 'vita',
            'last_name' => 'insurance',
            'phone' => '9400071734',
            'password' => bcrypt('123')
        ]);

        $role = Role::create(['name' => 'admin']);

        $permissions = Permission::pluck('id','id')->all();

        $role->syncPermissions($permissions);

        $user->assignRole([$role->id]);

        $enrollment = new Enrollment();
        $enrollment->en_number = $enrollmentNumber;
        $enrollment->user_id = $user->id;
        $enrollment->group_id = 1;
        $enrollment->status = 0;
        $enrollment->save();
    }
}
