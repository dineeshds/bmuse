<?php

use Illuminate\Database\Seeder;
use App\Models\Gender;
use App\Models\GroupInfo;
use App\Models\Role;

class GenderSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Gender::truncate();
        Gender::create(['gender' => 'Male']);
        Gender::create(['gender' => 'Female']);

        GroupInfo::truncate();
        GroupInfo::create([
            'name' => 'anj',
            'code' => 'ANJ123',
            'url' => 'test url',
            'status' => 1
            ]);

        Role::updateOrCreate([
            'name' => 'primary_user',
            'guard_name' => 'web'
        ]);

    }
}
