<?php

use Illuminate\Database\Seeder;
use App\Models\PlanPricing;
use App\Models\Tier;
use App\Models\Plan;
use App\Models\PlanPrimaryAge;
use App\Models\AgeRange;
use App\Models\PlanCategory;
use App\Models\PlanDependentAge;
use App\Models\PricingAddon;
use App\Models\PlanPricingAddon;
use App\Models\Questionnaire;
use App\Models\PlanQuestion;
use App\Models\Relation;
use App\Models\PlanGroup;
use App\Models\PlanEmployment;
use App\Models\PlanState;
use App\Models\PlanGender;
use App\Models\PlanTier;
use App\Models\Salary;
use App\Models\State;
use App\Models\PlanSalary;
use App\Models\Benefit;
use App\Models\PlanBenefit;
use App\Models\EmploymentType;
use App\Models\Agreement;
use Faker\Generator as Faker;

class PlanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        EmploymentType::truncate();
        EmploymentType::create(['employment_type' => 'Permanent']);
        EmploymentType::create(['employment_type' => 'Contract']);

        State::truncate();
        State::create(['state'=>'Florida', 'state_code'=>'FL']);

        Salary::truncate();
        Salary::create(['salary_start' => 0, 'salary_end' =>5000, 'status' => 1]);
        Salary::create(['salary_start' => 5000, 'salary_end' =>10000, 'status' => 1]);
        Salary::create(['salary_start' => 10000, 'salary_end' =>15000, 'status' => 1]);
        Salary::create(['salary_start' => 15000, 'salary_end' =>20000, 'status' => 1]);
        Salary::create(['salary_start' => 20000, 'salary_end' =>25000, 'status' => 1]);

        Tier::truncate();
        Tier::create(['tier' => 'You + Spouse', 'tier_code' => 'MEM-SP']);
        Tier::create(['tier' => 'You + family', 'tier_code' => 'MEM-FAM']);
        Tier::create(['tier' => 'Only Me', 'tier_code' => 'MEM']);
        Tier::create(['tier' => 'You + child', 'tier_code' => 'MEM-C']);
        Tier::create(['tier' => 'You + Parents', 'tier_code' => 'MEM-PA']);
        Tier::create(['tier' => 'You +  Mother', 'tier_code' => 'MEM-MO']);
        Tier::create(['tier' => 'You +  Father', 'tier_code' => 'MEM-FA']);

        Relation::truncate();
        Relation::create(['name' => 'Spouse']);
        Relation::create(['name' => 'Brother']);
        Relation::create(['name' => 'Sister']);
        Relation::create(['name' => 'Father']);
        Relation::create(['name' => 'Mother']);
        Relation::create(['name' => 'Child']);

        AgeRange::truncate();
        AgeRange::create(['age_start' => 0, 'age_end' => 18,'status' => 1]);
        AgeRange::create(['age_start' => 18, 'age_end' => 24,'status' => 1]);
        AgeRange::create(['age_start' => 25, 'age_end' => 28,'status' => 1]);
        AgeRange::create(['age_start' => 28, 'age_end' => 31,'status' => 1]);
        AgeRange::create(['age_start' => 32, 'age_end' => 37,'status' => 1]);
        AgeRange::create(['age_start' => 38, 'age_end' => 42,'status' => 1]);
        AgeRange::create(['age_start' => 43, 'age_end' => 47,'status' => 1]);

        PlanCategory::truncate();
        PlanCategory::create(['name' => 'Medical plan', 'description'=>'this is a medical plan', 'code' => 'MP123']);
        PlanCategory::create(['name' => 'Dental plan', 'description'=>'this is a dental plan', 'code' => 'DT']);

        Plan::truncate();
        Plan::create(['name' => 'Elite Blue Low', 'has_questionnaire' => 1, 'has_beneficiary' => 1,'description' => 'test', 'code' => 'ATP', 'pricing_type'=>1, 'status'=>1, 'plan_category_id'=>1]);
        Plan::create(['name' => 'Elite Blue High', 'has_questionnaire' => 1, 'description' => 'medicatal plan', 'code' => 'ATP1', 'pricing_type'=>1, 'status'=>1, 'plan_category_id'=>1]);
        Plan::create(['name' => 'MetLife Dental Silver', 'description' => 'dental', 'code' => 'DT1', 'pricing_type'=>1, 'status'=>1, 'plan_category_id'=>2]);

        Agreement::truncate();
        Agreement::create(['plan_id' =>1, 'type' => 'agreement', 'description' => 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.']);
        Agreement::create(['plan_id' =>2, 'type' => 'agreement', 'description' => 'Contrary to popular belief, Lorem Ipsum is not simply random text. It has roots in a piece of classical Latin literature from 45 BC, making it over 2000 years old. Richard McClintock, a Latin professor at Hampden-Sydney College in Virginia, looked up one of the more obscure Latin words, consectetur, from a Lorem Ipsum passage, and going through the cites of the word in classical literature, discovered the undoubtable source. Lorem Ipsum comes from sections 1.10.32 and 1.10.33 of "de Finibus Bonorum ']);
        Agreement::create(['plan_id' =>3, 'type' => 'agreement', 'description' => 'There are many variations of passages of Lorem Ipsum available, but the majority have suffered alteration in some form, by injected humour, or randomised words which don\'t look even slightly believable. If you are going to use a passage of Lorem Ipsum, you need to be sure there isn\'t anything embarrassing hidden in the middle of text. All the Lorem Ipsum generators on the Internet tend to repeat predefined chunks as necessary']);

        PlanPrimaryAge::truncate();
        PlanPrimaryAge::create(['plan_id' => 1, 'age_range_id' => 1]);
        PlanPrimaryAge::create(['plan_id' => 1, 'age_range_id' => 2]);
        PlanPrimaryAge::create(['plan_id' => 1, 'age_range_id' => 3]);
        PlanPrimaryAge::create(['plan_id' => 2, 'age_range_id' => 2]);
        PlanPrimaryAge::create(['plan_id' => 2, 'age_range_id' => 1]);
        PlanPrimaryAge::create(['plan_id' => 3, 'age_range_id' => 2]);
        PlanPrimaryAge::create(['plan_id' => 3, 'age_range_id' => 3]);

        PlanTier::truncate();
        PlanTier::create(['plan_id' => 1, 'tier_id' => 1 ]);
        PlanTier::create(['plan_id' => 1, 'tier_id' => 2]);
        PlanTier::create(['plan_id' => 1, 'tier_id' => 3]);
        PlanTier::create(['plan_id' => 1, 'tier_id' => 4]);
        PlanTier::create(['plan_id' => 1, 'tier_id' => 5]);
        PlanTier::create(['plan_id' => 1, 'tier_id' => 6]);
        PlanTier::create(['plan_id' => 1, 'tier_id' => 7]);
        PlanTier::create(['plan_id' => 2, 'tier_id' => 1]);
        PlanTier::create(['plan_id' => 2, 'tier_id' => 2]);
        PlanTier::create(['plan_id' => 2, 'tier_id' => 3]);
        PlanTier::create(['plan_id' => 2, 'tier_id' => 4]);
        PlanTier::create(['plan_id' => 2, 'tier_id' => 5]);
        PlanTier::create(['plan_id' => 2, 'tier_id' => 6]);
        PlanTier::create(['plan_id' => 3, 'tier_id' => 1]);
        PlanTier::create(['plan_id' => 3, 'tier_id' => 3]);
        PlanTier::create(['plan_id' => 3, 'tier_id' => 5]);
        PlanTier::create(['plan_id' => 3, 'tier_id' => 2]);
        PlanTier::create(['plan_id' => 3, 'tier_id' => 4]);

        PlanGender::truncate();
        PlanGender::create(['plan_id' => 1, 'gender_id' => 1]);
        PlanGender::create(['plan_id' => 1, 'gender_id' => 2]);
        PlanGender::create(['plan_id' => 2, 'gender_id' => 1]);
        PlanGender::create(['plan_id' => 2, 'gender_id' => 2]);
        PlanGender::create(['plan_id' => 3, 'gender_id' => 1]);
        PlanGender::create(['plan_id' => 3, 'gender_id' => 2]);

        PlanSalary::truncate();
        PlanSalary::create(['plan_id' => 1, 'salary_id' => 1]);
        PlanSalary::create(['plan_id' => 1, 'salary_id' => 2]);
        PlanSalary::create(['plan_id' => 1, 'salary_id' => 3]);
        PlanSalary::create(['plan_id' => 1, 'salary_id' => 4]);
        PlanSalary::create(['plan_id' => 1, 'salary_id' => 5]);
        PlanSalary::create(['plan_id' => 2, 'salary_id' => 1]);
        PlanSalary::create(['plan_id' => 2, 'salary_id' => 2]);
        PlanSalary::create(['plan_id' => 2, 'salary_id' => 3]);
        PlanSalary::create(['plan_id' => 2, 'salary_id' => 4]);
        PlanSalary::create(['plan_id' => 2, 'salary_id' => 5]);
        PlanSalary::create(['plan_id' => 3, 'salary_id' => 1]);
        PlanSalary::create(['plan_id' => 3, 'salary_id' => 2]);
        PlanSalary::create(['plan_id' => 3, 'salary_id' => 3]);
        PlanSalary::create(['plan_id' => 3, 'salary_id' => 4]);
        PlanSalary::create(['plan_id' => 3, 'salary_id' => 5]);

        PlanState::truncate();
        PlanState::create(['plan_id' => 1, 'state_id' => 1]);
        PlanState::create(['plan_id' => 2, 'state_id' => 1]);
        PlanState::create(['plan_id' => 3, 'state_id' => 1]);

        PlanEmployment::truncate();
        PlanEmployment::create(['plan_id' => 1, 'employment_type_id' => 1]);
        PlanEmployment::create(['plan_id' => 1, 'employment_type_id' => 2]);
        PlanEmployment::create(['plan_id' => 2, 'employment_type_id' => 1]);
        PlanEmployment::create(['plan_id' => 2, 'employment_type_id' => 2]);
        PlanEmployment::create(['plan_id' => 3, 'employment_type_id' => 1]);
        PlanEmployment::create(['plan_id' => 3, 'employment_type_id' => 2]);

        PlanGroup::truncate();
        PlanGroup::create(['plan_id' => 1, 'group_info_id' => 1]);
        PlanGroup::create(['plan_id' => 2, 'group_info_id' => 1]);
        PlanGroup::create(['plan_id' => 3, 'group_info_id' => 1]);

        PlanPricing::truncate();
        PlanPricing::create(['plan_id'=>1, 'plan_primary_age_id'=>1,'plan_tier_id'=>1,'plan_gender_id'=>1,'plan_salary_id' =>1, 'plan_state_id' => 1, 'plan_employment_id' =>1, 'plan_group_id' =>1 ,'price'=>200,'status'=>1,'start_date'=>'2020-12-10']);
        PlanPricing::create(['plan_id'=>1, 'plan_primary_age_id'=>0,'plan_tier_id'=>0,'plan_gender_id'=>0,'plan_salary_id' =>0 ,'plan_state_id' => 0, 'plan_employment_id' =>0, 'plan_group_id' =>0 ,'price'=>150,'status'=>1,'start_date'=>'2020-12-10']);
        PlanPricing::create(['plan_id'=>2, 'plan_primary_age_id'=>0,'plan_tier_id'=>0,'plan_gender_id'=>0,'plan_salary_id' =>0 ,'plan_state_id' => 0, 'plan_employment_id' =>0, 'plan_group_id' =>0 ,'price'=>140,'status'=>1,'start_date'=>'2020-12-10']);
        PlanPricing::create(['plan_id'=>3, 'plan_primary_age_id'=>0,'plan_tier_id'=>0,'plan_gender_id'=>0,'plan_salary_id' =>0 ,'plan_state_id' => 0, 'plan_employment_id' =>0, 'plan_group_id' =>0 ,'price'=>145,'status'=>1,'start_date'=>'2020-12-10']);
        PlanPricing::create(['plan_id'=>2, 'plan_primary_age_id'=>0,'plan_tier_id'=>2,'plan_gender_id'=>0,'plan_salary_id' =>0 ,'plan_state_id' => 0, 'plan_employment_id' =>0, 'plan_group_id' =>0 ,'price'=>250,'status'=>1,'start_date'=>'2020-12-10']);
        PlanPricing::create(['plan_id'=>3, 'plan_primary_age_id'=>0,'plan_tier_id'=>3,'plan_gender_id'=>0,'plan_salary_id' =>0 ,'plan_state_id' => 0, 'plan_employment_id' =>0, 'plan_group_id' =>0 ,'price'=>350,'status'=>1,'start_date'=>'2020-12-10']);
        PlanPricing::create(['plan_id'=>3, 'plan_primary_age_id'=>0,'plan_tier_id'=>4,'plan_gender_id'=>0,'plan_salary_id' =>0 ,'plan_state_id' => 0, 'plan_employment_id' =>0, 'plan_group_id' =>0 ,'price'=>450,'status'=>1,'start_date'=>'2020-12-10']);
        PlanPricing::create(['plan_id'=>3, 'plan_primary_age_id'=>0,'plan_tier_id'=>5,'plan_gender_id'=>0,'plan_salary_id' =>0 ,'plan_state_id' => 0, 'plan_employment_id' =>0, 'plan_group_id' =>0 ,'price'=>550,'status'=>1,'start_date'=>'2020-12-10']);
        PlanPricing::create(['plan_id'=>3, 'plan_primary_age_id'=>0,'plan_tier_id'=>6,'plan_gender_id'=>0,'plan_salary_id' =>0 ,'plan_state_id' => 0, 'plan_employment_id' =>0, 'plan_group_id' =>0 ,'price'=>650,'status'=>1,'start_date'=>'2020-12-10']);

        PricingAddon::truncate();
        PricingAddon::create(['name' => 'Tabaco User Surcharge - Employee', 'description' => 'test']);
        PricingAddon::create(['name' => 'Tabaco User Surcharge - Spouse/Domestic Partner', 'description' => 'test']);
        PricingAddon::create(['name' => 'Removal of $500 Medical Savings - Employee', 'description' => 'test']);
        PricingAddon::create(['name' => 'Removal of $500 Medical Savings - Spouse/Domestic Partner', 'description' => 'test']);
        PricingAddon::create(['name' => 'Wellness Screening and Assessment Savings - Employee', 'description' => 'test']);
        PricingAddon::create(['name' => 'Age sercharge', 'description' => 'test']);

        PlanPricingAddon::truncate();
        PlanPricingAddon::create(['plan_id' =>1 ,'pricing_addon_id'=>1, 'plan_dependent_age_id'=>1, 'plan_gender_id'=>1, 'price'=>20, 'status'=>1, 'type'=>1]);
        PlanPricingAddon::create(['plan_id' =>1 ,'pricing_addon_id'=>2, 'plan_dependent_age_id'=>1, 'plan_gender_id'=>1, 'price'=>10, 'status'=>1, 'type'=>1]);
        PlanPricingAddon::create(['plan_id' =>1 ,'pricing_addon_id'=>3, 'plan_dependent_age_id'=>1, 'plan_gender_id'=>1, 'price'=>10, 'status'=>1, 'type'=>1]);
        PlanPricingAddon::create(['plan_id' =>1 ,'pricing_addon_id'=>4, 'plan_dependent_age_id'=>1, 'plan_gender_id'=>1, 'price'=>10, 'status'=>1, 'type'=>1]);
        PlanPricingAddon::create(['plan_id' =>1 ,'pricing_addon_id'=>5, 'plan_dependent_age_id'=>1, 'plan_gender_id'=>1, 'price'=>30, 'status'=>1, 'type'=>0]);
        PlanPricingAddon::create(['plan_id' =>2 ,'pricing_addon_id'=>1, 'plan_dependent_age_id'=>1, 'plan_gender_id'=>1, 'price'=>20, 'status'=>1, 'type'=>1]);
        PlanPricingAddon::create(['plan_id' =>2 ,'pricing_addon_id'=>4, 'plan_dependent_age_id'=>1, 'plan_gender_id'=>1, 'price'=>60, 'status'=>1, 'type'=>1]);
        PlanPricingAddon::create(['plan_id' =>2 ,'pricing_addon_id'=>3, 'plan_dependent_age_id'=>1, 'plan_gender_id'=>1, 'price'=>15, 'status'=>1, 'type'=>1]);
        PlanPricingAddon::create(['plan_id' =>3 ,'pricing_addon_id'=>1, 'plan_dependent_age_id'=>1, 'plan_gender_id'=>1, 'price'=>22, 'status'=>1, 'type'=>1]);
        PlanPricingAddon::create(['plan_id' =>3 ,'pricing_addon_id'=>2, 'plan_dependent_age_id'=>1, 'plan_gender_id'=>1, 'price'=>10, 'status'=>1, 'type'=>0]);
        PlanPricingAddon::create(['plan_id' =>3 ,'pricing_addon_id'=>3, 'plan_dependent_age_id'=>1, 'plan_gender_id'=>1, 'price'=>16, 'status'=>1, 'type'=>0]);

        Questionnaire::truncate();
        Questionnaire::create(['question' => 'Heart attack, brain tumour, stroke, heart disease or heart problems?', 'type' => 'radio']);
        Questionnaire::create(['question' => 'Cancer, tumor, lymphoma, or any type of transplant?', 'type' => 'radio']);
        Questionnaire::create(['question' => 'Any surgery or hospitalization in the last 5 years, or any currently pending, planned or recommended?', 'type' => 'radio']);
        Questionnaire::create(['question' => 'Emphysema or COPD?', 'type' => 'radio']);
        Questionnaire::create(['question' => 'Kidney failure, dialysis, or disorder of the liver, stomach, pancreas, colon or bladder?', 'type' => 'radio']);
        Questionnaire::create(['question' => 'Seizures, epilepsy, haemophilia, sleep Apnea or blood disorder', 'type' => 'radio']);
        Questionnaire::create(['question' => 'Diabetes, endocrine, auto immune, chron\'s Disease or Arthritis, or pituitary disorder, growth disorder, lps, MS, AIDS, or HIV+?', 'type' => 'radio']);
        Questionnaire::create(['question' => 'Currently pregnant, premature delivery, or multiple births?', 'type' => 'radio']);
        Questionnaire::create(['question' => 'Are you taking or have you taken any medications in the last 12 months? If yes, you must list them below.', 'type' => 'multiple']);
        Questionnaire::create(['question' => 'Additional Questions', 'type' => 'textarea']);

        PlanQuestion::truncate();
        PlanQuestion::create(['plan_id' => 1, 'questionnaire_id' => 1, 'status' => 1]);
        PlanQuestion::create(['plan_id' => 1, 'questionnaire_id' => 3, 'status' => 1]);
        PlanQuestion::create(['plan_id' => 1, 'questionnaire_id' => 5, 'status' => 1]);
        PlanQuestion::create(['plan_id' => 1, 'questionnaire_id' => 6, 'status' => 1]);
        PlanQuestion::create(['plan_id' => 2, 'questionnaire_id' => 2, 'status' => 1]);
        PlanQuestion::create(['plan_id' => 2, 'questionnaire_id' => 3, 'status' => 1]);
        PlanQuestion::create(['plan_id' => 2, 'questionnaire_id' => 4, 'status' => 1]);
        PlanQuestion::create(['plan_id' => 2, 'questionnaire_id' => 5, 'status' => 1]);
        PlanQuestion::create(['plan_id' => 2, 'questionnaire_id' => 9, 'status' => 1]);
        PlanQuestion::create(['plan_id' => 2, 'questionnaire_id' => 1, 'status' => 1]);
        PlanQuestion::create(['plan_id' => 1, 'questionnaire_id' => 10, 'status' => 1]);

        Benefit::truncate();
        Benefit::create(['benefit' => 'plan type']);
        Benefit::create(['benefit' => 'Enrollment Deadline']);
        Benefit::create(['benefit' => 'Plan Availability']);
        Benefit::create(['benefit' => 'Network']);
        Benefit::create(['benefit' => 'Referrals']);
        Benefit::create(['benefit' => 'Deductible In-Network']);
        Benefit::create(['benefit' => 'Deductible Out-of-Network']);
        Benefit::create(['benefit' => 'Co-Insurance']);
        Benefit::create(['benefit' => 'Out of Pocket Max']);
        Benefit::create(['benefit' => 'Office Co-payments']);
        Benefit::create(['benefit' => 'Hospital (In Patient)']);
        Benefit::create(['benefit' => 'Prescription Benefits']);
        Benefit::create(['benefit' => 'Emergency Room']);
        Benefit::create(['benefit' => 'Emergency Medical Transportation']);
        Benefit::create(['benefit' => 'Chiropractic']);
        Benefit::create(['benefit' => 'Outpatient Mental Health']);
        Benefit::create(['benefit' => 'Inpatient Mental Health']);
        Benefit::create(['benefit' => 'Outpatient Imaging']);
        Benefit::create(['benefit' => 'X-Ray, Bloodwork']);
        Benefit::create(['benefit' => 'Urgent Care']);
        Benefit::create(['benefit' => 'Routine Child Eye Exam']);
        Benefit::create(['benefit' => 'Routine Child Dental Check-up']);
        Benefit::create(['benefit' => 'Durable Medical']);
        Benefit::create(['benefit' => 'Advanced Imaging']);
        Benefit::create(['benefit' => 'Home Health Care']);
        Benefit::create(['benefit' => 'Hospital Outpatient Facility']);
        Benefit::create(['benefit' => 'Physician and surgeon fees']);
        Benefit::create(['benefit' => 'Skilled Nursing Care']);
        Benefit::create(['benefit' => 'Rehabilitation Services']);
        Benefit::create(['benefit' => 'Hospice Services']);
        Benefit::create(['benefit' => 'Out-of-network']);
        Benefit::create(['benefit' => 'GENERAL EXCLUSIONS']);

        PlanBenefit::truncate();
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 1, 'coverage'=>'MM [Major Medical]']);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 2, 'coverage'=>'18th of month Prior to Effective date']);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 3, 'coverage'=>'All 50 States']);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 4, 'coverage'=>'Blue Card National PPO Network']);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 5, 'coverage'=>'No Referrals Required']);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 6, 'coverage'=>'$0 Single / $0 Family']);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 7, 'coverage'=>'$7,900 Single / $15,000 Family']);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 8, 'coverage'=> "In-Net: 40% Co-Insurance<br>Out-Net: Unlimited Single / Unlimited Family"]);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 9, 'coverage'=>'In-Net: $30 Primary / $40 Specialist<br>Out-Net: Deductible & Co-Insurance']);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 10, 'coverage'=>'In-Net: 40% Co-Insurance<br>Out-Net: Deductible & Co-Insurance<br>(Balance Paid at Plan In Network Rate)']);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 11, 'coverage'=>'Generic: $0<br>Brand preferred: 25%<br>Non-Preferred: 50%<br>Not Subject to Deductible']);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 12, 'coverage'=>'In-Net: $250.00 Copay / 40% Co-Insurance<br>Out-Net: $250.00 Copay / 40% Co-Insurance<br>(Balance Paid at Plan In Network Rate)']);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 13, 'coverage'=>'In-Net: 40% Co-Insurance<br>Out-Net: Deductible & Co-Insurance<br>(Balance Paid at Plan In Network Rate)']);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 14, 'coverage'=>'30 Visits Per Year']);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 15, 'coverage'=>'In-Net: $50 Copay<br>Out-Net: Deductible & Co-Insurance']);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 16, 'coverage'=>'In-Net: 40% Co-Insurance<br>Out-Net: Deductible & Co-Insurance<br>(Balance Paid at Plan In Network Rate)']);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 17, 'coverage'=>'In-Net: 40% Co-Insurance<br>Out-Net: Deductible & Co-Insurance']);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 18, 'coverage'=>'In-Net: 40% Co-Insurance<br>Out-Net: Deductible & Co-Insurance']);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 19, 'coverage'=>'In-Net: $50 Copay<br>Out-Net: Deductible & Co-Insurance']);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 20, 'coverage'=>'Covered']);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 21, 'coverage'=>'Covered']);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 22, 'coverage'=>'In-Net: 40% Co-Insurance<br>Out-Net: Deductible & Co-Insurance']);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 23, 'coverage'=>'In-Net: 40% Co-Insurance<br>Out-Net: Deductible & Co-Insurance']);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 24, 'coverage'=>'In-Net: 40% Co-Insurance<br>Out-Net: Deductible & Co-Insurance']);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 25, 'coverage'=>'In-Net: 40% Co-Insurance<br>Out-Net: Deductible & Co-Insurance']);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 26, 'coverage'=>'In-Net: 40% Co-Insurance<br>Out-Net: Deductible & Co-Insurance<br>(Balance Paid at Plan In Network Rate)']);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 27, 'coverage'=>'In-Net: 40% Co-Insurance<br>Out-Net: Deductible & Co-Insurance']);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 28, 'coverage'=>'In-Net: 40% Co-Insurance<br>Out-Net: Deductible & Co-Insurance']);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 29, 'coverage'=>'In-Net: 40% Co-Insurance<br>Out-Net: Deductible & Co-Insurance']);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 30, 'coverage'=>'In-Net: 40% Co-Insurance<br>Out-Net: Deductible & Co-Insurance<br>(Balance Paid at Plan In Network Rate)']);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 31, 'coverage'=>'125% of Medicare']);
        PlanBenefit::create(['plan_id' => 1, 'benefit_id' => 32, 'coverage'=>'Motor vehicle injuries, any and all related charges, including deductible arising from or related to motor vehicle accident. A Motor Vehicle is not limited to an automobile, truck or van. It includes motorcycle, moped, all-terrain vehicle, snowmobile and other recreational vehicles (including motorized scooters, etc.). o Please note, when you purchase or renew your automobile insurance you have the option of opting out of personal injury protection ("PIP") insurance. We urge you not to exercise that option. If you do, you will have no hospitalization or medical coverage if you or your dependent is involved in a motor vehicle accident. (Initial $25,000 of medical charges resulting from a motor vehicle accident.)

In addition to any limits described under the sections which describe the benefits, there are specific limitations and exclusions with regard to all benefits. No benefits are payable for:
•                      • Air Ambulance and Air Transportation
•                      • Abortion or maternity for dependents;
•                     • Elective abortion, except for an abortion when the eligible female\'s life would be endangered if the fetus was to be carried to term;
•                     • Acupuncture;
•                      • Adoption expenses;
•                     • Services provided for ambulette service;
•                      • Artificial mechanical organs;
•                      • Bio feedback;
•                     • CAT and/or MRI scans when ordered by a Chiropractor;
•                     • Clinic visits;
•                      • Cochlear implants or hearing aids;
•                     • Counseling: Family, Marital, or Sexual;
•                     • Contraceptive management;
•                      • Cosmetic and reconstructive surgery (except as required by the Women’s Health and Cancer Rights Act);
•                      • Custodial convalescent care;
• Dental services, except those required as the result of an accident and rendered within six months of the accident; o Furnished in any setting other than a dentist’s office for the prevention or correction of teeth irregularities and malocclusion of jaws by wire appliances, braces, or other mechanical aids, or any other care, repair, removal, replacement, or treatment of the teeth, or surrounding tissues, except, (1) When necessitated by damage to sound natural teeth or surrounding issues as a result of a covered injury, or (2) For the excision of impacted unerupted teeth or of a tumor or cyst, or incision and drainage of an abscess or cyst, or (3) For any other surgical procedure not involving any tooth structure, alveolar process, or gingival tissues;

• Domestic violence;
• Donor expenses;
• Drug testing;
• Education diagnosis (including but not limited to: development testing; ADHD, etc.);
• Endoscopic plantar fasiatom (heel spur);
• Experimental or obsolete procedures. The Fund will not pay for any procedure if it is not generally regarded as effective or if it is experimental in the sense that its effectiveness is not generally recognized;
• Genetic testing or counseling, chromosome testing and counseling, unless required with amniocentesis;
• Hair loss including but not limited to any and all medications. Wigs will be covered only according to the Woman’s Health and Cancer Rights Act;
• Hazardous activity, (i.e. jet skiing, motorcycling, ATV’s, snowmobiling, sky diving and bungee jumping);
• Holistic medical services;
• Confinement in a hospital owned and operated by the United States Government or any agency thereof; or for the service, treatment or supplies, furnished by or at the direction of the United States Government or any agency thereof;
• Confinement in a hospital owned or operated by a state, province or political subdivision, unless there is an unconditional requirement on the part of the covered person to pay such expenses without regard to
•               any liability against others, contractual or otherwise;
•               • Hyperbaric oxygen treatment;
•               • Hypnosis;
•                      • Immunizations, except as allowed in the Prescription Plan and administered in a participating pharmacy;
•                      • Impotence;
•                      • Treatment of infertility including but not limited to artificial insemination, in-vitro fertilization or reversal of elective sterilization;
•                      • Infa red coagulation of hemorrhoids;
•                     • An injury or an illness that is employment-related or that is eligible under the Worker\'s Compensation Law, Occupational Disease Law or similar laws. This exclusion does not apply to the Accidental Death and Dismemberment Benefit. If you fail to file for workmen’s compensation when injured on the job, the Fund will not be responsible for and charges in connection with that illness or injury;
•                      • An injury for which the Participant is entitled to recover from another person or organization;
•                      • For membership in, or fees, dues or charges incurred with regard to recreational facilities or fitness centers, even though prescribed by a physician;
•                     • Medical eye exam, when not performed by an ophthalmologist. (this does not include your vision exam through the Optical Benefits Plan;
• Motor vehicle injuries, any and all related charges, including deductible arising from or related to motor vehicle accident. A Motor Vehicle is not limited to an automobile, truck or van. It includes motorcycle, moped, all-terrain vehicle, snowmobile and other recreational vehicles (including motorized scooters, etc.). o Please note, when you purchase or renew your automobile insurance you have the option of opting out of personal injury protection ("PIP") insurance. We urge you not to exercise that option. If you do, you will have no hospitalization or medical coverage if you or your dependent is involved in a motor vehicle accident. (Initial $25,000 of medical charges resulting from a motor vehicle accident.)
• Any treatment or stay in a Nursing Home;
• Radial Keratotomy;
• Services or supplies, which are furnished for personal convenience such as air conditioners, humidifiers, physical fitness equipment, TENS units, muscle/bone stimulators or other such devices;
• Services rendered by a non-network or non-participating provider;
• Private duty nursing;
• Treatment of Temporomandibular Joint Syndrome (TMJ), Temporomandibular Joint Dysfunction or other condition of the joint linking the jaw bone (Mandible) and skull and the complex of muscles, nerves and other tissue related to the joint;
• Transsexual changes and/or surgeries and the treatment thereof;
• Varicose veins services and/or treatments (endovenous leg ablation procedures) if for cosmetic purposes;
• Services, supplies or treatment which are not prescribed as medically necessary by a physician or cost containment group. This exclusion also applies to any hospital confinement (or any part of a confinement) that is not recommended or approved by a physician;
• Obesity, weight control programs or any other type of service for weight control, whether it relates to illness or not;
• Any injury incurred during any organized sports or recreation program conducted by a school, college or other social organization and/or any injury coverable by coverage of said school, college, university or other social organization;
• Emergency Room Services to treat routine ailments, because you have no regular physician, or because it is late at night (and the need for treatment is not sudden or serious);
• Expenses incurred as result of participation in activities which would constitute a felony, riot, insurrection, or domestic violence and/or injury due to the use of guns, firearms, etc.;
•                      • Any services rendered by the claimant\'s immediate family;
•                     • Any charges which you or your dependents are not required to pay;
•                     • Bariatric Weight Reduction Surgery;
•                     • Transplants;
•                     • Pain management;
•                     • Podiatry services for funguses, toenails, weak, strained, flat feet, bunions, imbalances, corns, or calluses (except if patient is diabetic);
•                      • Treatment of corns, calluses or toenails, except removing nail roots and care prescribed by an M.D. or D.O. treating metabolic or peripheral vascular disease;
•                      • Any treatment not deemed medically necessary;
•                      • Unnecessary services or supplies;
• Services provided for vocational and/or educational training purposes; • Coverage for medical services provided outside the United States;
• Ultrasound and/or sonogram will be limited to 2 for each pregnancy; non-stress tests limited to 2 per pregnancy;
• Expenses incurred as a result of war or an act of war, declared or undeclared;
• Any service not listed as a covered benefit is excluded.']);

        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 1, 'coverage'=>'MM [Major Medical]']);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 2, 'coverage'=>'18th of month Prior to Effective date']);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 3, 'coverage'=>'All 50 States']);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 4, 'coverage'=>'Blue Card National PPO Network']);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 5, 'coverage'=>'No Referrals Required']);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 6, 'coverage'=>'$0 Single / $0 Family']);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 7, 'coverage'=>'$7,900 Single / $15,000 Family']);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 8, 'coverage'=> "In-Net: 40% Co-Insurance<br>Out-Net: Unlimited Single / Unlimited Family"]);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 9, 'coverage'=>'In-Net: $30 Primary / $40 Specialist<br>Out-Net: Deductible & Co-Insurance']);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 10, 'coverage'=>'In-Net: 40% Co-Insurance<br>Out-Net: Deductible & Co-Insurance<br>(Balance Paid at Plan In Network Rate)']);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 11, 'coverage'=>'Generic: $0<br>Brand preferred: 25%<br>Non-Preferred: 50%<br>Not Subject to Deductible']);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 12, 'coverage'=>'In-Net: $250.00 Copay / 40% Co-Insurance<br>Out-Net: $250.00 Copay / 40% Co-Insurance<br>(Balance Paid at Plan In Network Rate)']);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 13, 'coverage'=>'In-Net: 40% Co-Insurance<br>Out-Net: Deductible & Co-Insurance<br>(Balance Paid at Plan In Network Rate)']);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 14, 'coverage'=>'30 Visits Per Year']);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 15, 'coverage'=>'In-Net: $50 Copay<br>Out-Net: Deductible & Co-Insurance']);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 16, 'coverage'=>'In-Net: 40% Co-Insurance<br>Out-Net: Deductible & Co-Insurance<br>(Balance Paid at Plan In Network Rate)']);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 17, 'coverage'=>'In-Net: 40% Co-Insurance<br>Out-Net: Deductible & Co-Insurance']);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 18, 'coverage'=>'In-Net: 40% Co-Insurance<br>Out-Net: Deductible & Co-Insurance']);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 19, 'coverage'=>'In-Net: $50 Copay<br>Out-Net: Deductible & Co-Insurance']);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 20, 'coverage'=>'Covered']);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 21, 'coverage'=>'Covered']);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 22, 'coverage'=>'In-Net: 40% Co-Insurance<br>Out-Net: Deductible & Co-Insurance']);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 23, 'coverage'=>'In-Net: 40% Co-Insurance<br>Out-Net: Deductible & Co-Insurance']);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 24, 'coverage'=>'In-Net: 40% Co-Insurance<br>Out-Net: Deductible & Co-Insurance']);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 25, 'coverage'=>'In-Net: 40% Co-Insurance<br>Out-Net: Deductible & Co-Insurance']);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 26, 'coverage'=>'In-Net: 40% Co-Insurance<br>Out-Net: Deductible & Co-Insurance<br>(Balance Paid at Plan In Network Rate)']);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 27, 'coverage'=>'In-Net: 40% Co-Insurance<br>Out-Net: Deductible & Co-Insurance']);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 28, 'coverage'=>'In-Net: 40% Co-Insurance<br>Out-Net: Deductible & Co-Insurance']);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 29, 'coverage'=>'In-Net: 40% Co-Insurance<br>Out-Net: Deductible & Co-Insurance']);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 30, 'coverage'=>'In-Net: 40% Co-Insurance<br>Out-Net: Deductible & Co-Insurance<br>(Balance Paid at Plan In Network Rate)']);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 31, 'coverage'=>'125% of Medicare']);
        PlanBenefit::create(['plan_id' => 2, 'benefit_id' => 32, 'coverage'=>'Motor vehicle injuries, any and all related charges, including deductible arising from or related to motor vehicle accident. A Motor Vehicle is not limited to an automobile, truck or van. It includes motorcycle, moped, all-terrain vehicle, snowmobile and other recreational vehicles (including motorized scooters, etc.). o Please note, when you purchase or renew your automobile insurance you have the option of opting out of personal injury protection ("PIP") insurance. We urge you not to exercise that option. If you do, you will have no hospitalization or medical coverage if you or your dependent is involved in a motor vehicle accident. (Initial $25,000 of medical charges resulting from a motor vehicle accident.)

In addition to any limits described under the sections which describe the benefits, there are specific limitations and exclusions with regard to all benefits. No benefits are payable for:
•                      • Air Ambulance and Air Transportation
•                      • Abortion or maternity for dependents;
•                     • Elective abortion, except for an abortion when the eligible female\'s life would be endangered if the fetus was to be carried to term;
•                     • Acupuncture;
•                      • Adoption expenses;
•                     • Services provided for ambulette service;
•                      • Artificial mechanical organs;
•                      • Bio feedback;
•                     • CAT and/or MRI scans when ordered by a Chiropractor;
•                     • Clinic visits;
•                      • Cochlear implants or hearing aids;
•                     • Counseling: Family, Marital, or Sexual;
•                     • Contraceptive management;
•                      • Cosmetic and reconstructive surgery (except as required by the Women’s Health and Cancer Rights Act);
•                      • Custodial convalescent care;
• Dental services, except those required as the result of an accident and rendered within six months of the accident; o Furnished in any setting other than a dentist’s office for the prevention or correction of teeth irregularities and malocclusion of jaws by wire appliances, braces, or other mechanical aids, or any other care, repair, removal, replacement, or treatment of the teeth, or surrounding tissues, except, (1) When necessitated by damage to sound natural teeth or surrounding issues as a result of a covered injury, or (2) For the excision of impacted unerupted teeth or of a tumor or cyst, or incision and drainage of an abscess or cyst, or (3) For any other surgical procedure not involving any tooth structure, alveolar process, or gingival tissues;

• Domestic violence;
• Donor expenses;
• Drug testing;
• Education diagnosis (including but not limited to: development testing; ADHD, etc.);
• Endoscopic plantar fasiatom (heel spur);
• Experimental or obsolete procedures. The Fund will not pay for any procedure if it is not generally regarded as effective or if it is experimental in the sense that its effectiveness is not generally recognized;
• Genetic testing or counseling, chromosome testing and counseling, unless required with amniocentesis;
• Hair loss including but not limited to any and all medications. Wigs will be covered only according to the Woman’s Health and Cancer Rights Act;
• Hazardous activity, (i.e. jet skiing, motorcycling, ATV’s, snowmobiling, sky diving and bungee jumping);
• Holistic medical services;
• Confinement in a hospital owned and operated by the United States Government or any agency thereof; or for the service, treatment or supplies, furnished by or at the direction of the United States Government or any agency thereof;
• Confinement in a hospital owned or operated by a state, province or political subdivision, unless there is an unconditional requirement on the part of the covered person to pay such expenses without regard to
•               any liability against others, contractual or otherwise;
•               • Hyperbaric oxygen treatment;
•               • Hypnosis;
•                      • Immunizations, except as allowed in the Prescription Plan and administered in a participating pharmacy;
•                      • Impotence;
•                      • Treatment of infertility including but not limited to artificial insemination, in-vitro fertilization or reversal of elective sterilization;
•                      • Infa red coagulation of hemorrhoids;
•                     • An injury or an illness that is employment-related or that is eligible under the Worker\'s Compensation Law, Occupational Disease Law or similar laws. This exclusion does not apply to the Accidental Death and Dismemberment Benefit. If you fail to file for workmen’s compensation when injured on the job, the Fund will not be responsible for and charges in connection with that illness or injury;
•                      • An injury for which the Participant is entitled to recover from another person or organization;
•                      • For membership in, or fees, dues or charges incurred with regard to recreational facilities or fitness centers, even though prescribed by a physician;
•                     • Medical eye exam, when not performed by an ophthalmologist. (this does not include your vision exam through the Optical Benefits Plan;
• Motor vehicle injuries, any and all related charges, including deductible arising from or related to motor vehicle accident. A Motor Vehicle is not limited to an automobile, truck or van. It includes motorcycle, moped, all-terrain vehicle, snowmobile and other recreational vehicles (including motorized scooters, etc.). o Please note, when you purchase or renew your automobile insurance you have the option of opting out of personal injury protection ("PIP") insurance. We urge you not to exercise that option. If you do, you will have no hospitalization or medical coverage if you or your dependent is involved in a motor vehicle accident. (Initial $25,000 of medical charges resulting from a motor vehicle accident.)
• Any treatment or stay in a Nursing Home;
• Radial Keratotomy;
• Services or supplies, which are furnished for personal convenience such as air conditioners, humidifiers, physical fitness equipment, TENS units, muscle/bone stimulators or other such devices;
• Services rendered by a non-network or non-participating provider;
• Private duty nursing;
• Treatment of Temporomandibular Joint Syndrome (TMJ), Temporomandibular Joint Dysfunction or other condition of the joint linking the jaw bone (Mandible) and skull and the complex of muscles, nerves and other tissue related to the joint;
• Transsexual changes and/or surgeries and the treatment thereof;
• Varicose veins services and/or treatments (endovenous leg ablation procedures) if for cosmetic purposes;
• Services, supplies or treatment which are not prescribed as medically necessary by a physician or cost containment group. This exclusion also applies to any hospital confinement (or any part of a confinement) that is not recommended or approved by a physician;
• Obesity, weight control programs or any other type of service for weight control, whether it relates to illness or not;
• Any injury incurred during any organized sports or recreation program conducted by a school, college or other social organization and/or any injury coverable by coverage of said school, college, university or other social organization;
• Emergency Room Services to treat routine ailments, because you have no regular physician, or because it is late at night (and the need for treatment is not sudden or serious);
• Expenses incurred as result of participation in activities which would constitute a felony, riot, insurrection, or domestic violence and/or injury due to the use of guns, firearms, etc.;
•                      • Any services rendered by the claimant\'s immediate family;
•                     • Any charges which you or your dependents are not required to pay;
•                     • Bariatric Weight Reduction Surgery;
•                     • Transplants;
•                     • Pain management;
•                     • Podiatry services for funguses, toenails, weak, strained, flat feet, bunions, imbalances, corns, or calluses (except if patient is diabetic);
•                      • Treatment of corns, calluses or toenails, except removing nail roots and care prescribed by an M.D. or D.O. treating metabolic or peripheral vascular disease;
•                      • Any treatment not deemed medically necessary;
•                      • Unnecessary services or supplies;
• Services provided for vocational and/or educational training purposes; • Coverage for medical services provided outside the United States;
• Ultrasound and/or sonogram will be limited to 2 for each pregnancy; non-stress tests limited to 2 per pregnancy;
• Expenses incurred as a result of war or an act of war, declared or undeclared;
• Any service not listed as a covered benefit is excluded.']);

        PlanBenefit::create(['plan_id' => 3, 'benefit_id' => 1, 'coverage'=>'DENTAL']);
        PlanBenefit::create(['plan_id' => 3, 'benefit_id' => 2, 'coverage'=>'18th of month Prior to Effective date']);
        PlanBenefit::create(['plan_id' => 3, 'benefit_id' => 3, 'coverage'=>'AK, ID, LA, ME, MD, MT, NH, NM, OR, SD, WA NOT AVAILABLE']);
        PlanBenefit::create(['plan_id' => 3, 'benefit_id' => 4, 'coverage'=>'PDP Plus']);
        PlanBenefit::create(['plan_id' => 3, 'benefit_id' => 6, 'coverage'=>'100% of Negotiated Fee*']);
        PlanBenefit::create(['plan_id' => 3, 'benefit_id' => 7, 'coverage'=>'100% of Negotiated Fee*']);

    }
}
