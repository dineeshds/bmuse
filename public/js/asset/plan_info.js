$(document).ready(function () {

    //update personal info
    $(document).on('click',"#btn-save", function (e) {
        e.preventDefault();
        var oldTier = $("#old_tier").val();
        var newTier = $("#tier_id").val();
        if(oldTier != newTier){
            swal({
                title: "Are you sure?",
                text: "Once changed tier, you need to update your dependent information!.",
                icon: "warning",
                buttons: true,
                dangerMode: true,
            })
                .then((willDelete) => {
                    if (willDelete) {
                        $("#personal_info_modal_form").submit();
                        // swal("Your tier information has been updated!", {
                        //     icon: "success",
                        // });
                    } else {
                        swal("Changes are not saved!");
                    }
                });
        }else{
            $("#personal_info_modal_form").submit();
        }
    });

});

/**
 * edit personal info in a popup
 */
function edit_personal() {
    $("#personal_info_modal_form").parsley().reset();

    $.ajax({
        type: "get",
        url: url,

        success: function (data) {
            $("#phone").val(data.data.phone);
            $("#zip_code").val(data.data.zip_id);
            $("#dob").val(data.data.dob);
            $("#tier_id").val(data.data.tier_id);
        }
    });
}
