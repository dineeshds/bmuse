<?php

return [

    /*
    |--------------------------------------------------------------------------
    | General Helper
    |--------------------------------------------------------------------------
    |
    |
    */

    'tier' => [
        'MEM-SP' => [
            'title'         => 'Spouse Information',
            'btn'           => 0,
            'child_title'   => 0,
            'form_count'    => 1,
            'type'          => 'spouse'
        ],
        'MEM-FAM' => [
            'title'         => 'Family Information',
            'btn'           => 1,
            'child_title'   => 1,
            'form_count'    => 3,
            'type'          => 'family'
        ],
        'MEM-SC' => [
            'title'         => 'Spouse and Child  Information',
            'btn'           => 1,
            'child_title'   => 1,
            'form_count'    => 2,
            'type'          => 'child'
        ],
        'MEM-SCH' => [
            'title'         => 'Spouse and Children Information',
            'btn'           => 1,
            'child_title'   => 1,
            'form_count'    => 3,
            'type'          => 'children'
        ],
        'MEM-C' => [
            'title'         => 'Child Information',
            'btn'           => 1,
            'child_title'   => 1,
            'form_count'    => 1,
            'type'          => 'childonly'
        ],
        'MEM-CH' => [
            'title'         => 'Children Information',
            'btn'           => 1,
            'child_title'   => 1,
            'form_count'    => 2,
            'type'          => 'childreninfo'
        ],
        'MEM-PA' => [
            'title'         => 'Parents Information',
            'btn'           => 1,
            'child_title'   => 1,
            'form_count'    => 2,
            'type'          => 'parents'
        ],
        'MEM-MO' => [
            'title'         => 'Mother Information',
            'btn'           => 1,
            'child_title'   => 1,
            'form_count'    => 1,
            'type'          => 'mother'
        ],
        'MEM-FA' => [
            'title'         => 'Father Information',
            'btn'           => 1,
            'child_title'   => 1,
            'form_count'    => 1,
            'type'          => 'father'
        ],

    ],
    'zip_validator' => env('ZIP_VALIDATE_URL', 'https://zip.getziptastic.com/v2/US'),

    'relation' => [

           'Father' => [
               'status'  => 1,
               'name'    => 'father'
                ],
            'Mother' => [
            'status'  => 2,
            'name'    => 'mother'
                 ],
             'Spouse' => [
            'status'  => 3,
            'name'    => 'spouse'
                 ],
          'Child' => [
            'status'  => 4,
            'name'    => 'child'
        ],

    ],

];
