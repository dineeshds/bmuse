@extends('admin.layouts.app')
@section('content')
    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
        <!-- Content Header (Page header) -->
        <section class="content-header">
            <div class="container-fluid">
                <div class="row mb-2">
                    <div class="col-sm-6">
                        <h1>Manage Relations</h1>
                    </div>
                    <div class="col-sm-6">
                        <ol class="breadcrumb float-sm-right">
                            <li class="breadcrumb-item"><a href="{{url('admin/admin-home')}}">Dashboard</a></li>
                            <li class="breadcrumb-item active">Relations</li>
                        </ol>
                    </div>
                </div>
            </div><!-- /.container-fluid -->
        </section>

        <!-- Main content -->
        <section class="content">
            <div class="container-fluid">
                <div class="row">
                    <div class="col-12">
                        <!-- /.card -->

                        <div class="card">
                            <div class="card-header">
                                <h3 class="card-title">Relation lists</h3>
                                <a href="#" class="btn btn-info" style="float: right" data-toggle="modal" data-target="#modal-default">Add New</a><br>
                            </div>
                            <!-- /.card-header -->
                            <div class="card-body">
                                <table id="relation_table" class="table table-bordered table-striped">
                                    <thead>
                                    <tr>
                                        <th>Name</th>
                                        <th>Code</th>
                                        <th>Action</th>
                                    </tr>
                                    </thead>

                                    <tfoot>
                                    <tr>
                                        <th>Name</th>
                                        <th>Code</th>
                                        <th>Action</th>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                            <!-- /.card-body -->
                        </div>
                        <!-- /.card -->
                    </div>
                    <!-- /.col -->
                </div>
                <!-- /.row -->
            </div>
            <!-- /.container-fluid -->
        </section>
        <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    @include('admin.general.relation.modal')
@endsection

@push('css')
    <link rel="stylesheet" href="{{ asset('plugins/datatables.net-bs/css/dataTables.bootstrap.min.css') }}">
    <link rel="stylesheet" href="{{asset('AdminLTE/plugins/sweetalert2-theme-bootstrap-4/bootstrap-4.min.css')}}">
@endpush

@push('js')
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.4.1/js/bootstrap.min.js"></script>
    <!-- Bootstrap Confirmation -->
    <script src="{{asset('js/asset/alert/sweetalert.min.js')}}"></script>
    <!-- DataTables  & Plugins -->
    <script src="{{asset('AdminLTE/plugins/datatables/jquery.dataTables.min.js')}}"></script>
    <script src="{{asset('AdminLTE/plugins/datatables-bs4/js/dataTables.bootstrap4.min.js')}}"></script>
    <script src="{{asset('AdminLTE/plugins/datatables-responsive/js/dataTables.responsive.min.js')}}"></script>
    <script src="{{asset('AdminLTE/plugins/datatables-responsive/js/responsive.bootstrap4.min.js')}}"></script>
    <script src="{{asset('AdminLTE/plugins/datatables-buttons/js/dataTables.buttons.min.js')}}"></script>
    <script src="{{asset('AdminLTE/plugins/datatables-buttons/js/buttons.bootstrap4.min.js')}}"></script>
    <script src="{{asset('AdminLTE/plugins/jszip/jszip.min.js')}}"></script>
    <script src="{{asset('AdminLTE/plugins/pdfmake/pdfmake.min.js')}}"></script>
    <script src="{{asset('AdminLTE/plugins/pdfmake/vfs_fonts.js')}}"></script>
    <script src="{{asset('AdminLTE/plugins/datatables-buttons/js/buttons.html5.min.js')}}"></script>
    <script src="{{asset('AdminLTE/plugins/datatables-buttons/js/buttons.print.min.js')}}"></script>
    <script src="{{asset('AdminLTE/plugins/datatables-buttons/js/buttons.colVis.min.js')}}"></script>
    <script type="text/javascript">
        $(document).ready(function () {
            $(document).on('click', '#modal-default', function () {

            });
        });

        $(function () {

            $("#relation_table").DataTable({
                "processing": true,
                "lengthChange": true,
                "serverSide": true,
                "dom": '<"dt-buttons"Bf><"clear">lirtp',
                "paging": true,
                "scrollX": false,
                "responsive":true,
                "autoWidth": false,
                "ajax": {
                    url: "{{ route('relation.create') }}",
                },
                columns: [
                    { data: 'name', name: 'name' },
                    { data: 'code', name: 'code' },
                    { data: 'action', name: 'action', orderable: false, searchable: false}
                ],

                "buttons": [
                    "copy",
                    "csv",
                    "excel",
                    "pdf",
                    "print",
                    "colvis"
                ],
                "fnDrawCallback": function (oSettings) {
                    $(".btn-confirmation").click(function () {
                        swal({
                            title: "Are you sure ?",
                            icon: "warning",
                            buttons: true,
                            dangerMode: true,
                        }).then((willDelete) => {
                            if (willDelete) {
                                var status = $(this).data('value');
                                var url = $(this).data('url');
                                changeStatus(url, status);
                            }
                        });
                    });
                }
            }).buttons().container().appendTo('#example1_wrapper .col-md-6:eq(0)');
        });

        function changeStatus(url, status)
        {
            var Toast = Swal.mixin({
                toast: true,
                position: 'top-end',
                showConfirmButton: false,
                timer: 3000
            });
            $.ajax({
                url: url,
                data: {
                    "_token" : "{{ csrf_token() }}",
                    "status" : status
                },
                type: 'DELETE',
                success: function(result) {
                    if(result) {
                        $('#relation_table').DataTable().ajax.reload();
                    }
                },
                complete: function(){
                    Toast.fire({
                        icon: 'success',
                        title: 'Successfully changed status.'
                    })
                }
            });
        }

        function edit_relation(id)
        {
            var url = "{{ url('admin/relation') }}/:id/edit";

            $.ajax({
                type: "get",
                url : url.replace(':id', id),
                success:function(data)
                {
                    var response = data.data;
                    $("#relation-update").attr('action', '{{url('admin/relation')}}/'+id);
                    $("#up_name").val(response.name);
                    $("#up_code").val(response.code);
                }
            });
        }
    </script>
@endpush
