<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
<html xmlns="http://www.w3.org/1999/xhtml">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <title>A Simple Responsive HTML Email</title>
    <style type="text/css">
        body {margin: 0; padding: 0; min-width: 100%!important;  background-image: url("http://bmusewebsites.s3.amazonaws.com/test/background.jpg");}
        .mail-content {
            position: absolute;
            top:0;
            bottom: 0;
            left: 0;
            right: 0;
            margin: auto;
            width: 600px;
            padding-bottom:50px;
            color: #f8f5ec;
            background-color: white;
            background-repeat: repeat-y no-repeat;background-position: 50% 50%;
            font-family:Georgia, Times, serif;  font-size:14px; color:#333333;
            justify-content: center;
        }
        .mail-content-area {
            position: absolute;
            top:0;
            bottom: 0;
            left: 0;
            right: 0;
            margin: auto;
            width: 500px;
            padding-bottom:50px;
            color: #f8f5ec;
            justify-content: center;
            background-repeat: repeat-y no-repeat;background-position: 50% 50%;
            font-family:Georgia, Times, serif;  font-size:14px; color:#333333;
        }
        .separator {
            position: absolute;
            top:0;
            bottom: 0;
            left: 0;
            right: 0;
            margin: auto;
            position: relative;
            width: 100%;
            background-color: #BDBAB5;
            height: 2px;
            justify-content: center;
        }
        .space {
            padding-bottom:20px;
        }
    </style>
</head>
<body>
<div class="mail-content">
    <div class="mail-content-area">
        <img src="http://bmusewebsites.s3.amazonaws.com/test/logo.gif" alt="Logo">
        <div class="space"></div>
        <div class="separator"></div>
        <div class="space"></div>
            Dear <b>{{$news['name']}}</b> <br><br>

            {{$news['body']}}
        <div class="space"></div>
        <div class="separator"></div>
    </div>
</div>
</body>
</html>
