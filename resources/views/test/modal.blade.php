<div class="modal fade" id="edit-modal-default">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h4 class="modal-title">Add New Gender</h4>
                <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                    <span aria-hidden="true">&times;</span>
                </button>
            </div>
            <div class="modal-body">
                <form action="{{url('subscribers')}}" method="post" id="gender-save" data-parsley-validate="">
                    @csrf
                    <div class="form-group">
                        <label for="name">Subject</label>
                        <input type="text" class="form-control" required placeholder="subject" name="letter[subject]" id="subject" value="" />
                    </div>
                    <div class="form-group">
                        <label for="name">Body</label>
                        <textarea class="form-control" required placeholder="body" name="letter[body]" id="body" value="" ></textarea>
                    </div>
                    <input type="hidden" name="letter[sub_id]" id="sub_id">
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Cancel</button>
                        <button type="submit" class="btn btn-info">Save</button>
                    </div>
                </form>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
